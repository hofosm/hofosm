#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::TransImg;

use strict;
use warnings;
use Image::Magick;

use constant DEBUG => 0;


our $VERSION = '1.00';


sub doville {
    # Read a set of image and create a composite one with all of them
    #
    #
    # - $sx : number of images on x
    # - $sy : number of images on y
    #
    # - $width : composite image width
    # - $height : composite image height
    #
    # - corresponding zoom level
    #
    my ($tilesdir, $htmldir, $img, $type, $sx, $sy, $deltax, $deltay, $width, $height, $zoom, $debug) = @_;

    my ($w, $h) = intermediateSize($width, $height);
    
    my ($nx, $ny) = nbTiles ($w, $h);

    my $dirname = sprintf("%s/tiles/%s", $htmldir, $zoom);

    my $dirtiles = "$tilesdir/$zoom";

    my $filename = sprintf("%s/%s-%s.png", $dirname, $img, $type);

    if ( ! -f $filename ) {

	checkDir($dirname);

	printf "do : %s a %dx%d at %d,%d by size %dx%d final %dx%d\n", $filename, $nx, $ny, $sx, $sy, $w, $h, $width, $height;
	
	my $dest = createNewImage($nx, $ny);
	
	my ($x, $y) = (0,0);
	
	while ($x < $nx) {
	    $y = 0;
	    while ( $y < $ny ) {
		
		my $sfilename = sprintf("%s/%s/%s-%s.png", $dirtiles, ($sx+$x), ($sy+$y), $type);
		printf " - read : %d,%d %s\n", $x,$y,$sfilename  if $debug;
	
		my $subtile = imgRead($sfilename);

		imgCompose(\$dest, $subtile, 256 * $x, 256 * $y);

		$y++;
	    }    
	    $x++;	    
	}

	imgCrop(\$dest, $width, $height, 0, 0);
		
	addNotice(\$dest);
    
	$dest->Set(compression=>30);
	my $res = $dest->Write(filename=>$filename, compression=>'Zip',quality=>'0');
	die $res if $res;
    }		    
}

sub imgCompose {
    my ($img, $part, $x, $y) = @_;

    # %opts is defined like that because of bug on perl-mode 

    my %opts = ( image => $part, 
		 compose => "Over", 
		 x => $x );
    
    $opts{y} = $y;
		    
    $$img->Composite( %opts  );
}

sub imgRead {
    my ($filename) = @_;

    my $img = Image::Magick->new();

    my $ret = $img->ReadImage($filename);
    die $ret if $ret;

    return $img;
}


sub addNotice {
    my ($dest) = @_;

    my $strdate = "Generated on " . localtime() ;

    my $height = $$dest->Get('height');
	
    my %opts = ( font => 'DejaVu-Sans-Book',
		 rotate => 270,
		 text => $strdate,
		 x => 10 );

    $opts{y} = ($height - 5) ;

    $$dest->Annotate(%opts);
}

sub intermediateSize {

    my ($width, $height) = @_;

    my ($w, $h) = (0, 0);

    while ( $width > $w) {
	$w = $w + 256;
    }
    
    while ( $height > $h ) {
	$h = $h + 256;
    }
	
    return ($w, $h);

}

sub nbTiles {

    my ($width, $height) = @_;

    my ($w, $h) = ($width / 256, $height / 256);

    return ($w, $h);
}


sub createNewImage {
    #
    # Create a new Image
    #
    # - $w : width
    # - $h : height
    #
    my ($w, $h) = @_;

    printf "Create new Image %d,%d\n", $w, $h if DEBUG;

    die unless ($w * $h > 0);

    my $size = (256 * $w)."x".(256 * $h);
	
    my $dest=Image::Magick->new;

    $dest->Set(size=>$size,
	       colorspace=>'RGB',
	       depth=>'8',
	       type=>'Palette',
	       magick=>'PNG',
	);
    
    $dest->ReadImage('xc:#f0f0f0');

    return $dest;
}

sub imgCrop {
    #
    # Crop an Image
    #
    # - $w : width
    # - $h : height
    #
    my ($img, $w, $h, $offset_x, $offset_y) = @_;

    my $geom = sprintf("%dx%d+%d+%d", $w, $h, $offset_x, $offset_y);

    $$img->Crop( geometry => $geom );
    $$img->Set( page=>'0x0+0+0' );
}


sub checkDir {
    #
    # If the dir does not exists create it
    #
    my ($dirname) = @_;

    if (! -d $dirname) {
	mkdir($dirname);
    }
}

1;

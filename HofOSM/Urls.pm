#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::Urls;

use strict;
use warnings;
use Math::Trig;
use Math::Trig ":pi";
require LWP::UserAgent;

our $VERSION = '1.00';

sub fetchTiles {
    my ($tilesdir, $x, $y, $z) = @_;

    fetchTilesRend($tilesdir, "tiles", $x, $y, $z);
    fetchTilesRend($tilesdir, "gg", $x, $y, $z);
}

sub fetchTilesRend {
    my ($dir, $type, $x, $y, $z) = @_;

    for (my $i = $x - 1; $i <= $x + 1 ; $i++ ) {

	for (my $j = $y -1 ; $j <= $y + 1 ; $j++) {
	    fetchTile($dir, $type, $i, $j, $z);
	}
    }
}

sub fetchTile {
    my ($dir, $type, $x, $y, $z) = @_;

    my $url = "";
    my $imgname = tiledir($dir, $type, $z, $x, $y);
   
    if ($type eq 'tiles') {
	$url = osmurl($z, $x, $y);
    } else {
	$url = ggmurl($z, $x, $y);
    }

    if (! -f $imgname) {
	
	printf "Fetch %s %s\n", $imgname, $url;

	my $ua = LWP::UserAgent->new(timeout => 30);
	
	my $response = $ua->request(HTTP::Request->new('GET', $url));
	
	if ($response->is_success) {
	    my $img = $response->content;
	    
	    open ('TF',">$imgname");
	    print TF $img;
	    close(TF);
	    
	}
	elsif ($response->code == 404)  {
	    print "file not found 404\n";
	}
    }
}

sub tiledir {
    my ($base, $type, $z, $x, $y) = @_;

    if (! -d $base) {
	printf "Error %s dir does not exists\n", $base;
	exit 1
    }

    checkDir("$base/$z");
    checkDir("$base/$z/$x");

    return "$base/$z/$x/$y-$type.png";
}

sub checkDir {
    #
    # If the dir does not exists create it
    #
    my ($dirname) = @_;

    if (! -d $dirname) {
	mkdir($dirname);
    }
}


sub osmurl {
    my ($z, $x, $y) = @_;

    my $url = sprintf("http://tile.openstreetmap.org/%s/%s/%s.png", $z, $x, $y);

    return $url;
}

sub ggmurl {
    my ($z, $x, $y) = @_;

    my $url = sprintf("http://mt0.google.com/vt/x=%s&y=%s&z=%s&s=G", $x, $y, $z);

    return $url;
}


sub deg2num {
    my ($lon_deg, $lat_deg, $zoom) = @_;

    my $lat_rad = deg2rad($lat_deg);
    my $n = 2.0 ** $zoom;
    my $xtile = int(($lon_deg + 180.0) / 360.0 * $n);
    my $ytile = int((1.0 - log(tan($lat_rad) + (1 / cos($lat_rad))) / pi) / 2.0 * $n);
    
    return ($xtile, $ytile, $zoom);
}

sub urlname {
    # TODO : find a lib that generate a valid url base from a string

    my ($data) = @_;

    $data =~ s/\s/_/g;
    $data =~ s/'//g;

    return $data;
}


1;

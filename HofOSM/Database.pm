#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::Database;

use DBI;
use strict;

use constant DEBUG => 0;

sub addNewTown {
    my ($dbfile, $pays, $name, $lon, $lat, $zoom) = @_;

    my $dbh = _dbConnect($dbfile);

    $dbh->do("INSERT INTO towns (pays, name, lon, lat, zoom) VALUES ('$pays', '$name', $lon, $lat, $zoom)");
    
    if ($dbh->err()) { die "$DBI::errstr\n"; }
    
    $dbh->commit();
    $dbh->disconnect();
}

sub dbCreate {

    my ($dbfile) = @_;

    printf "Create database %s\n", $dbfile if DEBUG;

    my $dbh = _dbConnect($dbfile);

    my $query = "CREATE TABLE towns (id integer PRIMARY KEY ASC , pays varchar(50), name varchar(255), lon float, lat float, zoom integer, width integer, height integer, offsetx integer, offsety integer, username varchar(50), UNIQUE (pays, name) ); ";

    $dbh->do($query);
    
    if ($dbh->err()) { die "$DBI::errstr\n"; }
    
    $dbh->commit();
    $dbh->disconnect();

    return 0;
}

sub dbInsertHash {

    my ($dbfile, $town) = @_;

    my $dbh = _dbConnect($dbfile);

    my $name = %$town->{'name'};

    $name =~ s/'/''/g;

    my $query = sprintf("INSERT INTO towns (pays, name, lon, lat, zoom) VALUES ('%s','%s','%s','%s','%s'); ", 
			%$town->{'pays'},
			$name,
			%$town->{'lon'},
			%$town->{'lat'},
			%$town->{'zoom'});

    $dbh->do($query);

    if ($dbh->err()) { 
	if ($DBI::err == 19) {
	    # violation d'une clé unique
	    printf "Unique key %s\n", $DBI::errstr;
	} else {
	    die "$DBI::errstr ($DBI::err)\n"; 
	}
    }
    
    $dbh->commit();
    $dbh->disconnect();

    return 0;
}

sub dbShowAll {
    my ($dbfile) = @_;

    my $dbh = _dbConnect($dbfile);

    my $all = $dbh->selectall_arrayref("SELECT id, pays, name, lon, lat, zoom FROM towns");

    if ($dbh->err()) { die "$DBI::errstr\n"; }

    printf "     %-20s %-25s %s %s\n", 'Pays','Ville','Lon','Lat';

    foreach my $row (@$all) {
	my ($id, $pays, $name, $lon, $lat) = @$row;
	printf "%4d %-20s %-30s %9s %9s\n", $id, $pays, $name, $lon, $lat;
    }

    

    
    $dbh->commit();
    $dbh->disconnect();
}


sub dbTownsById {
    my ($dbfile) = @_;
    my @datas;
    my $dbh = _dbConnect($dbfile);

    my $all = $dbh->selectall_arrayref("SELECT pays, name, lon, lat, zoom FROM towns ORDER BY id ASC");

    if ($dbh->err()) { die "$DBI::errstr\n"; }

    foreach my $row (@$all) {
	push(@datas, { 'pays' => @$row[0], 'name' => @$row[1], 'lon' => @$row[2], 'lat' => @$row[3], 'zoom' => @$row[4] });
    }

    $dbh->commit();
    $dbh->disconnect();

    return @datas;
}

sub _dbConnect {
    my ($dbfile) = @_;

    my $dbargs = {AutoCommit => 0,
                  PrintError => 1};
    
    my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile","","",$dbargs);


    return $dbh;
}

1;

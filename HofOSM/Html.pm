#!/usr/bin/perl
#
# Copyright (C) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::Html;

use strict;
use HTML::Template;

sub genIndex {
    #
    # Generate HTML pages
    #
    # titre : town name
    # 
    # lat : latitude position of concerned town
    # lon : longitude position of concerned town

    my ($htmldir, $tmplfile, $titre, $image, $zoom, $lon, $lat, $pays, $last, $fwidth, $fheight, %nextp) = @_;

    my $template = newTemplate($tmplfile);

    $template->param(TITLE => $titre);
    $template->param(LON => $lon);
    $template->param(LAT => $lat);
    $template->param(ZOOM => $zoom);
    $template->param(IMAGE => $image);

    $template->param(IMAGE_WIDTH => $fwidth);
    $template->param(IMAGE_HEIGHT => $fheight);

    $template->param(ALTNEXT => $nextp{'altnext'});
    $template->param(ALTPREV => $nextp{'altnext'});
    $template->param(URLNEXT => $nextp{'urlnext'});
    $template->param(URLPREV => $nextp{'urlprev'});

    $template->param(PAYS => $pays);

    my $filename = sprintf("%s/%s.html", $htmldir, $image);
   
    open('FD', ">$filename");

    $template->output(print_to => *FD);

    printf "Write %s\n", $filename;

    close(FD);

    if ($last) {
	my $indexname = sprintf("%s/index.html", $htmldir);
   
	open('FD', ">$indexname");

	$template->output(print_to => *FD);

	printf "Write index file is %s\n", $filename;

	close(FD);
    }
}

sub newTemplate {
    my ($filename) = @_;
    return HTML::Template->new(filename => $filename);
}

1;

#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::ImportFile;

use strict;

sub importFile {
    my ($filename, $dbfile) = @_;

    die "File $ filename does not exists" unless ( -f $filename );

    my @datas = CSV($filename);

    foreach my $data (@datas) {
	HofOSM::Database::dbInsertHash($dbfile, $data);
    }

    return 0;
}


sub CSV {
    #
    # Read datas from CSV files
    #
    my ($filename) = @_;
    my @datas = ();;
    
    # Open data file
    open(SM, "<$filename") || die;

    while (my $line = <SM>) {
    
	# suppress header
	my @fields = split(";",$line);

	if ( scalar(@fields) == 5 ) {

	    my $zone;

	    $zone->{'pays'} = cleanchar($fields[0]);
	    $zone->{'name'} = cleanchar($fields[1]);
	    $zone->{'lon'} = cleanchar($fields[2]);
	    $zone->{'lat'} = cleanchar($fields[3]);
	    $zone->{'zoom'} = cleanchar($fields[4]);
	
	    push(@datas, $zone);
	}
    }
    close(SM);

    return @datas;
}

sub cleanchar
{
    # Nettoyage de la chaîne de caractère
    my ($val) = @_;

    $val =~ s/"//g;
    return $val;
}

1;

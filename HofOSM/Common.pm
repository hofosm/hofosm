#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package HofOSM::Common;

use strict;
use Text::Unaccent;

our $VERSION = '1.00';

# Size of generated images (integer)
# number of original tile



sub generateTowns {
    #
    # Generate all HTML files and PNG images
    #
    my ($tilesdir, $htmldir, $tmpldir, $fwidth, $fheight, @towns) = @_;

    my @list = ();

    foreach my $elmt (@towns) {
	push(@list, $elmt->{'name'});
    }

    my $nbelmt = scalar(@list);
    my $i = 0;

    foreach my $elmt (@towns) {
	# is it the last town
	my $last = ($nbelmt - 1 == $i ) ? 1 : 0;

	my $next = $list[$i+1];
	my $prev = $list[$i-1];

	$prev = $list[$nbelmt - 1] if ($i == 0) ;
	$next = $list[0] if ($i == $nbelmt - 1) ;

	town($tilesdir, $htmldir, $tmpldir,
	     $elmt->{'name'}, 
	     $elmt->{'lon'}, 
	     $elmt->{'lat'}, 
	     $elmt->{'zoom'}, 
	     $next, 
	     $prev,
	     $fwidth,
	     $fheight,
	     $elmt->{'pays'}, $last);
	$i++;
    }
}


sub town {
    my ($tilesdir, $htmldir, $tmpldir, $name, $lon, $lat, $zoom, $next, $prev, $fwidth, $fheight, $pays, $last) = @_;

    my $img = HofOSM::Urls::urlname(unac_string("UTF-8", $name));

    my $zi = $zoom;

    while ($zi <= $zoom) {
	    
	my ($x, $y, $z) = HofOSM::Urls::deg2num($lon, $lat, $zi);
	
	HofOSM::Urls::fetchTiles($tilesdir, $x, $y, $z);

	HofOSM::TransImg::doville ($tilesdir, $htmldir, $img, "tiles", $x-1, $y-1, 0, 0, $fwidth, $fheight, $zi, 1);
	HofOSM::TransImg::doville ($tilesdir, $htmldir, $img, "gg", $x-1, $y-1, 0, 0, $fwidth, $fheight, $zi, 1);
	$zi++;
    }

    #
    # Previous and Next images
    #
    my %urls;
    
    $urls{'altprev'} = $prev;
    $urls{'altnext'} = $next;
    $urls{'urlprev'} = sprintf("%s.html",HofOSM::Urls::urlname(unac_string("UTF-8", $prev)));
    $urls{'urlnext'} = sprintf("%s.html",HofOSM::Urls::urlname(unac_string("UTF-8", $next)));

    # generate html pages
    
    HofOSM::Html::genIndex($htmldir, $tmpldir.'/index.tmpl', $name, $img, $zoom, $lon, $lat, $pays, $last, $fwidth, $fheight, %urls);
}


return 1;

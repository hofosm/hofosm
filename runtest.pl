#!/usr/bin/perl

use HofOSM::Database;
use HofOSM::Html;
use HofOSM::ImportFile;
use HofOSM::TransImg;
use HofOSM::Urls;

use Tests::Database;
use Tests::Html;
use Tests::ImportFile;
use Tests::TransImg;
use Tests::Urls;

# run all the test
Test::Class->runtests;

#!/usr/bin/perl
#
# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Image::Magick;
use Text::Unaccent;
use Getopt::Long;
use Pod::Usage;

require LWP::UserAgent;

use HofOSM::TransImg;
use HofOSM::Urls;
use HofOSM::Html;
use HofOSM::Common;
use HofOSM::Database;
use HofOSM::ImportFile;


use constant DBFILE => "towns.sqlite";


use constant { TILESDIR => "tiles", # Where we generate the composed tiles
	       HTMLDIR => "htdocs",
	       TMPLDIR => "templates",
	       FWIDTH => 600, 
	       FHEIGHT => 750 
};

my %Options = {'images' => 1, 'html' => 1 };
my $ok = GetOptions(\%Options, "html", "images!", "tilesdir=s", "htmldir=s", "create-db", "show", "import-file=s", "help");

# print help message
pod2usage(1) if $Options{'help'};

# quit on bad options
pod2usage(1) unless $ok;

my $tilesdir = defined($Options{'tilesdir'}) ? $Options{'tilesdir'} : TILESDIR;
my $htmldir = defined($Options{'htmldir'}) ? $Options{'htmldir'} : HTMLDIR;
my $tmpldir = defined($Options{'tmpldir'}) ? $Options{'tmpldir'} : TMPLDIR;
my $dbfile = defined($Options{'dbfile'}) ? $Options{'dbfile'} : DBFILE;

if ( defined($Options{'create-db'}) ) {
    HofOSM::Database::dbCreate($dbfile);    
    exit 0;
}

if (! -f $dbfile ) {
    printf "ERROR : Database file '%s' not found\n", $dbfile;
    pod2usage(1);
}

if ( defined($Options{'show'}) ) {
    HofOSM::Database::dbShowAll($dbfile);    
    exit 0;
}

if ( defined($Options{'import-file'}) ) {
    HofOSM::ImportFile::importFile($Options{'import-file'}, $dbfile);
    exit 0;
}

# Read towns from database
my @datas = HofOSM::Database::dbTownsById($dbfile);

# Generate HTML and images
HofOSM::Common::generateTowns($tilesdir, $htmldir, $tmpldir, FWIDTH, FHEIGHT, @datas);


__END__

=head1 NAME

hofosm.pl - Hall Of Fame OSM data manipulation

=head1 SYNOPSIS

hofosm.pl [options]

=head1 OPTIONS

=over 8

=item B<--html>

Do the HTML files generation (default)

=item B<--images>

Do the images generation, can be negative with --no-images

=item B<--create-db>

Create the sqlite3 database file

=item B<--show>

Show all zones presents in database file

=item B<--tilesdir>

Change the default directory to generate the tiles

=item B<--htmldir>

Change the default directory where html files are written


=item B<--import-file> FILE

Importe filename FILE, FILE is a csv file

=item B<--help>

Print this message

=back

=head1 DESCRIPTION

B<This program> will read datas from a sqlite database, fetch tiles
from webserver, generate bigger tiles and HTML files.

=cut

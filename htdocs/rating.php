<?php // -*- php -*-
//
// Copyright (C) 2011 Loic Dachary <loic@dachary.org>
//
// This software's license gives you freedom; you can copy, convey,
// propagate, redistribute and/or modify this program under the terms of
// the GNU Affero General Public License (AGPL) as published by the Free
// Software Foundation (FSF), either version 3 of the License, or (at your
// option) any later version of the AGPL published by the FSF.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program in a file in the toplevel directory called
// "AGPLv3".  If not, see <http://www.gnu.org/licenses/>.
//

class rating
{
  const MAX_URL_SIZE		=	512;
  const MAX_COMMENT_SIZE	=	128;
  const MAX_ROWS		=	256;

  function __construct()
  {
    $pathinfo = pathinfo(__FILE__);
    $this->path = realpath($pathinfo['dirname']) . "/hofosm.sqlite";
    $this->db = null;
    $this->max_rows = self::MAX_ROWS;
  }

  function __destruct()
  {
    if($this->db)
      sqlite_close($this->db);
  }

  function db_check()
  {
    $exists = file_exists($this->path);
    $this->db = sqlite_open($this->path, 0666, $error);
    if($error)
      throw new Exception('sqlite_open(' . $this->path . '): ' . $error);
    if(!$exists) {
      sqlite_query($this->db, "CREATE TABLE urls (id INTEGER PRIMARY KEY, url TEXT, comment VARCHAR(128), votes INTEGER DEFAULT 0, created DATE)");
      sqlite_query($this->db, "CREATE UNIQUE INDEX urls_key ON urls (url)");
      sqlite_query($this->db, "CREATE INDEX urls_idx ON urls (votes, created)");
      sqlite_query($this->db, "CREATE TABLE ips (id INTEGER, ip VARCHAR(15))");
      sqlite_query($this->db, "CREATE INDEX ips_idx ON ips (id, ip)");
    }
  }

  function handle($get, $post, $ip)
  {
    try {
      $this->db_check();

      if(isset($get['submit'])) {
        return $this->submit($post);
      } else if(isset($get['rate'])) {
        return $this->rate($post);
      } else {
        return $this->get($get);
      }
    } catch(Exception $exception) {
      return '{"error": "' . str_replace('"', '\"', $exception->getMessage()) . '"}';
    }
  }

  function check($name, $value, $max_size)
  {
    $value_size = strlen($value);
    if($value_size > $max_size)
      throw new Exception($name . " too long: " . $value_size . " > " . $max_size);
    if($value_size <= 0)
      throw new Exception($name . " is empty");
    return $value;
  }

  function submit($post)
  {
    $url = $this->check('url', $post['url'], self::MAX_URL_SIZE);
    $comment = $this->check('comment', $post['comment'], self::MAX_COMMENT_SIZE);
    sqlite_query($this->db, "INSERT INTO urls (url, comment, created) VALUES ('" . sqlite_escape_string($url) . "', '" . sqlite_escape_string($comment) . "', date('now'))", SQLITE_NUM, $error);
    if($error)
      throw new Exception($error);
    return '{}';
  }
    
  function rate($post)
  {
    $id = intval($post['id']);
    sqlite_query($this->db, "UPDATE urls SET votes = votes + 1 WHERE id = " . $id);
    return '{}';
  }
    
  function get($get)
  {
    return '{"rows":' . json_encode(sqlite_array_query($this->db, "SELECT * FROM urls ORDER BY votes DESC, created DESC LIMIT " . $this->max_rows, SQLITE_NUM)) . '}';
  }
}

?>

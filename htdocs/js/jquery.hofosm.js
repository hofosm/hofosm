//
//     Copyright (C) 2011 Loic Dachary <loic@dachary.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
(function($) {
    if(!String.prototype.supplant) {
        //
        // Douglas Crockford douglas@crockford.com snippet
        //
        String.prototype.supplant = function (o) {
            return this.replace(/{([^{}]*)}/g,
                                function (a, b) {
                                    var r = o[b];
                                    return typeof r === 'string' || typeof r === 'number' ? r : a;
                                }
                                );
        };
    }

    $.hofosm = {
        url: "hofosm.php",
        error: function(what) { alert(what); },
        setTimeout: function(cb, delay) { return window.setTimeout(cb, delay); },
        ajax: function(o) { return jQuery.ajax(o); },
        thickbox_complete: function() {},
        thickbox_closed: function() {},
        html: '' +
            '<div class=\'hofosm\'>' +
            ' <div class=\'hofosm_submit\'>' + 
            '  <input type=\'text\' name=\'url\' size=\'80\' value=\'URL\'></input>' + 
            '  <input type=\'text\' name=\'comment\' size=\'80\' value=\'comment\'></input>' + 
            '  <input type=\'submit\' name=\'submit\'></input>' +
            ' </div>' +
            ' <div class=\'hofosm_urls\'>' +
            '  <table class=\'tablesorter {widthFixed: true, sortlist: [[3,1],[2,1]]}\' cellspacing=\'1\'>' + // column 3=votes in descending order and column 2=created in descending order
            '   <thead>' +
            '    <tr><th>ID</th><th>SUBMISSION</th><th>CREATED</th><th>VOTES</th></tr>' +
            '   </thead>' +
            '   <tbody></tbody>' +
            '  </table>' +
            '  <div class=\'hofosm_pager pager\'>' +
	    '   <form>' +
	    '    <span class=\'first\'>first</span> ' +
	    '    <span class=\'prev\'>prev</span> ' +
	    '    <input type=\'text\' class=\'pagedisplay\'/> ' + 
	    '    <span class=\'next\'>next</span> ' +
	    '    <span class=\'last\'>last</span> ' +
	    '    <select class=\'pagesize\'> ' +
	    '     <option selected=\'selected\' value=\'10\'>10</option> ' +
	    '     <option value=\'20\'>20</option> ' +
	    '     <option value=\'30\'>30</option> ' +
	    '     <option value=\'40\'>40</option> ' +
            '    </select> ' +
            '   </form>' +
            '  </div>' +
            ' </div>' +
            '</div>',
        row: '<tr><td>{id}</td><td><a href=\'{url}\' title=\'Watch the candidate and vote\'>{comment}</a></td><td>{created}</td><td>{votes}</td></tr>'
    };

    $.fn.hofosm = function(options) {
        var opts = $.extend({}, $.hofosm, options);
        return this.each(function() {
            var $this = $(this);
            $this.append(opts.html);
            var xhr_error = function(xhr, status, error) {
                opts.error(error);
            };
            // rate the url
            var rate = function(id) {
                var success = function(data, status) {
                    if('error' in data)
                        opts.error(data.error);
                    else
                        $.hofosm.setTimeout(function() { get(); }, 30);
                };
                $.hofosm.ajax({
                    async: false,
                    timeout: 30000,
                    url: opts.url + '?rate=1',
                    type: 'POST',
                    data: 'id=' + id,
                    dataType: 'json',
                    global: false,
                    success: success,
                    error: xhr_error
                    });	
            };
            // populate the list of URLs
            var get = function() {
                var success = function(data, status) {
                    if('error' in data) {
                        opts.error(data.error);
                    } else {
                        var rows = [];
                        for(var i = 0; i < data.rows.length; i++)
                        {
                            var row = data.rows[i];
                            rows.push(opts.row.supplant({ 'id': row[0],
                                                          'url': row[1],
                                                          'comment': row[2],
                                                          'votes': row[3],
                                                          'created': row[4] }));
                        }
                        $('tbody', $this).html(rows.join('\n'));
                        $('tr').each(function() {
                            var id = $('td:nth-child(1)', this).html();
                            $('a', this).colorbox({
                                width:"80%",
                                height:"80%",
                                iframe:true,
                                title: function() {
                                    return "Click here to vote in favor";
                                },
                                onClosed: function() {
                                    $.hofosm.thickbox_closed();
                                },
                                onComplete: function() {
                                    $('#cboxTitle').click(function() {
                                        $.fn.colorbox.close();
                                        $.hofosm.setTimeout(function() { rate(id); }, 30);
                                    });
                                    $.hofosm.thickbox_complete();
                                }
                            });
                        });
                        $('table', $this)
                            .tablesorter()
                            .tablesorterPager({ container: $('.hofosm_pager', $this) });
                    }
                };
                $.hofosm.ajax({
                    async: false,
                    timeout: 30000,
                    url: opts.url,
                    type: 'GET',
                    dataType: 'json',
                    global: false,
                    success: success,
                    error: xhr_error
                    });
            };
            $.hofosm.setTimeout(function() { get(); }, 30);
            // input form
            $('input[type=submit]', this).click(function() {
                var success = function(data, status) {
                    if('error' in data)
                        opts.error(data.error);
                    else
                        $.hofosm.setTimeout(function() { get(); }, 30);
                };
                var parent = $(this).parent();
                var url = encodeURIComponent($('input[name=url]', parent).val());
                var comment = encodeURIComponent($('input[name=comment]', parent).val());
                $.hofosm.ajax({
                    async: false,
                    timeout: 30000,
                    url: opts.url + '?submit=1',
                    type: 'POST',
                    data: 'url=' + url + '&comment=' + comment,
                    dataType: 'json',
                    global: false,
                    success: success,
                    error: xhr_error
                    });	
            });
            return this;
        });
    };

})(jQuery);

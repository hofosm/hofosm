module("hofosm");

function setup() {
    $.hofosm.setTimeout = function(cb, delay) { return window.setTimeout(cb, delay); };
    $.hofosm.ajax = function(o) { return jQuery.ajax(o); };
    $.hofosm.thickbox_complete = function() {};
    $.hofosm.thickbox_closed = function() {};
}

test("hofosm get", function() {
    setup();
    expect(4);
    stop();

    $.hofosm.setTimeout = function(cb, delay) {
        window.setTimeout(function() {
            cb();
            var tr = $('.hofosm_urls tbody tr');
            equal(tr.length, 10); // only 10 rows out of 14 because of the pager
            // url1 is first although it is last in the list below, because tablesorter shows
            // the rows with most votes first
            equal($('.hofosm_urls tbody tr:first td:nth-child(1)').html(), '1'); // compare id
            equal($('.hofosm_pager option:selected').val(), '10');
            start();
        }, delay);
    };

    $.hofosm.ajax = function(options) {
        equal(options.type, 'GET');
        options.success({"rows":[
            ["2","url2","comment2","0","2011-01-27"],
            ["1","url1","comment1","1","2011-01-27"],
            ["3","url3","comment3","0","2000-01-01"],
            ["4","url3","comment3","0","2000-01-01"],
            ["5","url3","comment3","0","2000-01-01"],
            ["6","url3","comment3","0","2000-01-01"],
            ["7","url3","comment3","0","2000-01-01"],
            ["8","url3","comment3","0","2000-01-01"],
            ["9","url3","comment3","0","2000-01-01"],
            ["10","url3","comment3","0","2000-01-01"],
            ["11","url3","comment3","0","2000-01-01"],
            ["12","url3","comment3","0","2000-01-01"],
            ["13","url3","comment3","0","2000-01-01"],
            ["14","url3","comment3","0","2000-01-01"]
        ]}, true);
    };

    $('#qunit-fixture').hofosm();
});


test("hofosm view and rate", function() {
    setup();
    expect(10);
    stop();

    var ajax_done = false;
    var thickbox_done = false;

    $.hofosm.thickbox_closed = function() {
        ok(true, "thickbox_closed");
        thickbox_done = true;
        if(ajax_done)
            start();
    };        

    var setTimeout_7 = function(cb, delay) {
        ok(false, "setTimeout_7");
    };

    var ajax_6 = function(options) {
        equal(options.url, 'hofosm.php', "ajax_5");
        equal(options.type, 'GET', "ajax_5");

        ajax_done = true;
        if(thickbox_done)
            start();
    };

    var setTimeout_5 = function(cb, delay) {
        window.setTimeout(function() {
            ok(true, "setTimeout_5");
            $.hofosm.ajax = ajax_6;
            $.hofosm.setTimeout = setTimeout_7;
            cb();
        }, delay);
    };

    var ajax_4 = function(options) {
        $.hofosm.ajax = ajax_6;
        equal(options.url, 'hofosm.php?rate=1', "ajax_5");
        equal(options.type, 'POST', "ajax_4");
        ok(options.url.indexOf('rate=') >= 0, "ajax_4");
        options.success({}, true);
    };

    var setTimeout_3 = function(cb, delay) {
        window.setTimeout(function() {
            ok(true, "setTimeout_3");
            $.hofosm.ajax = ajax_4;
            $.hofosm.setTimeout = setTimeout_5;
            cb();
        }, delay);
    };

    var click = function() {
        $.hofosm.setTimeout = setTimeout_3;
        $('#cboxTitle').click();
    };

    var ajax_2 = function(options) {
        equal(options.type, 'GET', "ajax_2");
        options.success({"rows":[
            ["2","file:///","comment1","0","2011-01-27"]
        ]}, true);
        $('.hofosm_urls tbody tr:first td:nth-child(2) a').click();
        var element = $.fn.colorbox.element();
        equals(element.length, 1, "ajax_2");
        $.hofosm.thickbox_complete = click;
    };

    var setTimeout_1 = function(cb, delay) {
        window.setTimeout(function() {
            $.hofosm.ajax = ajax_2;
            cb();
        }, 30);
    };

    $.hofosm.setTimeout = setTimeout_1;

    $('#qunit-fixture').hofosm();
});

test("hofosm submit", function() {
    setup();
    expect(2);

    expected_url = 'UR&L';
    expected_comment = 'COMM=ENT';

    $.hofosm.ajax = function(options) {
        equal(options.type, 'POST');
        equal(options.data, 'url=UR%26L&comment=COMM%3DENT');
    };
    $('#qunit-fixture').hofosm();
    $('.hofosm_submit input[name=url]').val(expected_url);
    $('.hofosm_submit input[name=comment]').val(expected_comment);
    $('.hofosm_submit input[type=submit]').click();
});

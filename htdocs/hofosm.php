<?php // -*- php -*-
//
// Copyright (C) 2011 Loic Dachary <loic@dachary.org>
//
// This software's license gives you freedom; you can copy, convey,
// propagate, redistribute and/or modify this program under the terms of
// the GNU Affero General Public License (AGPL) as published by the Free
// Software Foundation (FSF), either version 3 of the License, or (at your
// option) any later version of the AGPL published by the FSF.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program in a file in the toplevel directory called
// "AGPLv3".  If not, see <http://www.gnu.org/licenses/>.
//

// add url1 and url2
// curl --form comment=comment1 --form url=url1 http://localhost/pool/hofosm.php?submit=1
// curl --form comment=comment2 --form url=url2 http://localhost/pool/hofosm.php?submit=1
// vote for url2 assuming it has id=2
// curl --form id=2 http://localhost/pool/hofosm.php?rate=1
// display database content
// curl http://localhost/pool/hofosm.php

require_once 'rating.php';

$rating = new rating();

print $rating->handle($_GET, $_POST, $_SERVER["REMOTE_ADDR"]);
?>

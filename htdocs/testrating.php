<?php // -*- php -*-
//
// Copyright (C) 2011 Loic Dachary <loic@dachary.org>
//
// This software's license gives you freedom; you can copy, convey,
// propagate, redistribute and/or modify this program under the terms of
// the GNU Affero General Public License (AGPL) as published by the Free
// Software Foundation (FSF), either version 3 of the License, or (at your
// option) any later version of the AGPL published by the FSF.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program in a file in the toplevel directory called
// "AGPLv3".  If not, see <http://www.gnu.org/licenses/>.
//

require_once 'PHPUnit/Framework/TestCase.php';

require_once 'rating.php';

class testrating extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
      error_reporting(E_ALL);
      if(file_exists('test.sqlite'))
         unlink('test.sqlite');
      $this->rating = new rating();
      $this->rating->path = 'test.sqlite';
    }

    protected function tearDown()
    {
      unlink('test.sqlite');
    }

    public function test00_dbcheck()
    {
      $this->rating->db_check();
      $this->assertFileExists($this->rating->path);
      $this->assertEquals(0, sqlite_single_query($this->rating->db, "SELECT COUNT(*) FROM urls", true));
    }

    public function test00_dbcheck_fail()
    {
      $caught = false;
      try {
        $this->rating->path = "/unlikely";
        $this->rating->db_check();
      } catch(Exception $exception) {
        $this->assertContains('sqlite_open', $exception->getMessage());
        $caught = true;
      }
      $this->assertTrue($caught);
    }

    public function test01_submit()
    {
      $this->rating->db_check();
      $this->assertEquals(0, sqlite_single_query($this->rating->db, "SELECT COUNT(*) FROM urls", true));

      // successfull insertion with escaping
      $url = "UR'L1";
      $comment = "co'ment";
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => $url, 'comment' => $comment ), 'IP1');
      $this->assertEquals($result, '{}');
      $this->assertEquals($url, sqlite_single_query($this->rating->db, "SELECT url FROM urls", true));
      $this->assertEquals($comment, sqlite_single_query($this->rating->db, "SELECT comment FROM urls", true));
      $this->assertEquals(0, sqlite_single_query($this->rating->db, "SELECT votes FROM urls", true));
      $this->assertEquals(sqlite_single_query($this->rating->db, "SELECT date('now')"), sqlite_single_query($this->rating->db, "SELECT created FROM urls", true));

      // url too long
      $url = "SOMETHING";
      for($i = 0; $i < rating::MAX_URL_SIZE; $i++)
        $url .= "+";
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => $url, 'comment' => 'comment' ), 'IP1');
      $this->assertContains('"error"', $result);
      $this->assertContains("url too long", $result);

      // url empty
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'comment' => 'comment' ), 'IP1');
      $this->assertContains('"error"', $result);
      $this->assertContains("url is empty", $result);

      // comment too long
      $comment = "SOMETHING";
      for($i = 0; $i < rating::MAX_COMMENT_SIZE; $i++)
        $comment .= "+";
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url', 'comment' => $comment ), 'IP1');
      $this->assertContains('"error"', $result);
      $this->assertContains("comment too long", $result);

      // comment empty
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url' ), 'IP1');
      $this->assertContains('"error"', $result);
      $this->assertContains("comment is empty", $result);

      // fail to insert the same URL twice
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url', 'comment' => 'comment'), 'IP1');
      $this->assertEquals($result, '{}');
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url', 'comment' => 'comment'), 'IP1');
      $this->assertContains('"error"', $result);
      $this->assertContains("column url is not unique", $result);
    }

    public function test02_rate()
    {
      $this->rating->db_check();
      // noop
      $result = $this->rating->handle(array( 'rate' => 1 ), array( 'id' => 'abc' ), 'IP1');

      // rate
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url', 'comment' => 'comment' ), 'IP1');
      $this->assertEquals($result, '{}');
      $id = sqlite_single_query($this->rating->db, "SELECT id FROM urls WHERE url = 'url'", true);
      $this->assertEquals(0, sqlite_single_query($this->rating->db, "SELECT votes FROM urls WHERE id = " . $id, true));
      $result = $this->rating->handle(array( 'rate' => 1 ), array( 'id' => $id ), 'IP1');
      $this->assertEquals($result, '{}');

      $this->assertEquals(1, sqlite_single_query($this->rating->db, "SELECT votes FROM urls WHERE id = " . $id, true));
    }

    public function test03_get()
    {
      $this->rating->db_check();

      // no rows
      $json = $this->rating->handle(array( ), array( ), 'IP1');
      $this->assertEquals($json, '{"rows":[]}');

      // one row
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url1', 'comment' => 'comment1' ), 'ip1');
      $this->assertEquals($result, '{}');
      $json = $this->rating->handle(array( ), array( ), 'IP1');
      $rows = json_decode($json)->rows;
      $this->assertEquals(1, count($rows));
      $this->assertEquals('url1', $rows[0][1]);

      // two rows, limited to one row
      $this->rating->max_rows = 1;
      $result = $this->rating->handle(array( 'submit' => 1 ), array( 'url' => 'url2', 'comment' => 'comment2' ), 'ip2');
      sqlite_query($this->rating->db, "UPDATE urls SET created = date('now', '-1 day') WHERE url = 'url2'");
      $this->assertEquals($result, '{}');
      $json = $this->rating->handle(array( ), array( ), 'IP1');
      $rows = json_decode($json)->rows;
      $this->assertEquals(1, count($rows));
      $this->assertEquals('url1', $rows[0][1]);
      
      // url2 is first because its created date is sooner
      sqlite_query($this->rating->db, "UPDATE urls SET created = date('now', '+1 day') WHERE url = 'url2'");
      $json = $this->rating->handle(array( ), array( ), 'IP1');
      $rows = json_decode($json)->rows;
      $this->assertEquals(1, count($rows));
      $this->assertEquals('url2', $rows[0][1]);

      // one vote to the url1 has precedence over a more recent URL
      $id = sqlite_single_query($this->rating->db, "SELECT id FROM urls WHERE url = 'url1'", true);      
      $result = $this->rating->handle(array( 'rate' => 1 ), array( 'id' => $id ), 'IP1');
      $this->assertEquals($result, '{}');
      $json = $this->rating->handle(array( ), array( ), 'IP1');
      $rows = json_decode($json)->rows;
      $this->assertEquals(1, count($rows));
      $this->assertEquals('url1', $rows[0][1]);
    }
}

//
// Interpreted by emacs
// Local Variables:
// compile-command: "rm -fr phpunit ; mkdir phpunit ; phpunit --report phpunit testrating.php"
// End: 
?>

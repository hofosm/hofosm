# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Tests::Database;

use base qw(Test::Class);
use Test::More;

use strict;

use HofOSM::Database;

# setup methods are run before every test method. 
sub make_fixture : Test(setup) {
    unlink("/tmp/titi.sqlite") if ( -f "/tmp/titi.sqlite");
}


# Database

sub test_dbCreate : Test { 
    is(HofOSM::Database::dbCreate("/tmp/titi.sqlite") , 0 , "Database creation" ); 
}

sub test_dbInsertHash : Test { 
    HofOSM::Database::dbCreate("/tmp/titi.sqlite"); 

    my %town = { 'name' => 'Paris',
		 'pays' => 'France',
		 'lon' => 3.3,
		 'lat' => -45.99,
		 'zoom' => 8 };

    is(HofOSM::Database::dbInsertHash("/tmp/titi.sqlite", \%town) , 0 , "Database insert Hash" ); 
}





# teardown methods are run after every test method.
sub teardown : Test(teardown) {
    unlink("/tmp/titi.sqlite") if ( -f "/tmp/titi.sqlite");
};


1;

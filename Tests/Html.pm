# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Tests::Html;

use base qw(Test::Class);
use Test::More;

use strict;

use HofOSM::Html;

use constant TMPLNAME => "/tmp/test.tmpl";

# setup methods are run before every test method. 
sub make_fixture : Test(setup) {
    open (TMPL, ">".TMPLNAME);
    print TMPL '<html><TMPL_VAR NAME="FOO"></html>';
    close(TMPL)
}

sub test_newTemplate : Test(1) { 
    my $tmpl = HofOSM::Html::newTemplate(TMPLNAME);
    $tmpl->param('FOO' => "bar");

    is($tmpl->param('FOO') , "bar", "new template" ) ; 

}


# teardown methods are run after every test method.
sub teardown : Test(teardown) {
    unlink(TMPLNAME) if ( -f TMPLNAME);
};



1;

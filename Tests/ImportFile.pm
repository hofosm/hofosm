# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Tests::ImportFile;

use base qw(Test::Class);
use Test::More;

use strict;
use Image::Magick;

use HofOSM::ImportFile;

sub test_cleanchar : Test(4) { 
    is(HofOSM::ImportFile::cleanchar('"toto"') , 'toto', "remove double quotes" ); 
    is(HofOSM::ImportFile::cleanchar('toto') , 'toto', "remove double quotes" ); 
    is(HofOSM::ImportFile::cleanchar('"toto') , 'toto', "remove double quotes" ); 
    is(HofOSM::ImportFile::cleanchar('to"to') , 'toto', "remove double quotes" ); 
}


1;

# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Tests::Urls;

use base qw(Test::Class);
use Test::More;

use strict;
use Image::Magick;

use HofOSM::Urls;


# Modules Urls

sub test_osmurl : Test { is(HofOSM::Urls::osmurl(3,3,3) , "http://tile.openstreetmap.org/3/3/3.png", "ok url osm" ); }
sub test_ggmurl : Test { is(HofOSM::Urls::ggmurl(3,3,3) , "http://mt0.google.com/vt/x=3&y=3&z=3&s=G", "ok url ggmap" ); }

sub test_tiledir : Test { is(HofOSM::Urls::tiledir("/tmp","yoyo",26,24,25) , "/tmp/26/24/25-yoyo.png", "url de la tuile" ); }

sub test_deg2num : Test(3) { 
    my ($x, $y , $z) = HofOSM::Urls::deg2num(-0.46142,49.88047, 14);

    is($x , 8171, "conversion coord tile number X" ) ; 

    is($y , 5565, "conversion coord tile number Y" ); 

    is($z , 14, "conversion coord tile number Z" ); 
}

sub test_urlname : Test(6) { 
    is(HofOSM::Urls::urlname("foo bar"),    "foo_bar", "urlname" ); 
    is(HofOSM::Urls::urlname("foobar "),    "foobar_", "urlname" ); 
    is(HofOSM::Urls::urlname(" foo bar"),   "_foo_bar", "urlname" ); 
    is(HofOSM::Urls::urlname("foo'")  ,  "foo", "urlname" ); 
    is(HofOSM::Urls::urlname("bar 'foo' "),   "bar_foo_", "urlname" ); 
    is(HofOSM::Urls::urlname("''bar 'foo"),   "bar_foo", "urlname" ); 
}

1;

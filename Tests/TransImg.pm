# Copyright (C) 2010-2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Tests::TransImg;

use base qw(Test::Class);
use Test::More;

use strict;
use Image::Magick;

use HofOSM::TransImg;

#
sub test_intermediateSize : Test(2) { 
    my ($w, $h) = HofOSM::TransImg::intermediateSize(500, 200);
    is($w , 512, "intermediate width for 500" ); 
    is($h , 256, "intermediate height for 200" ); 
}

sub test_intermediateSize : Test(2) { 
    my ($w, $h) = HofOSM::TransImg::intermediateSize(512, 768);
    is($w, 512, "intermediate width for 512 : $w" ); 
    is($h, 768, "intermediate height for 768 : $h" ); 
}

sub test_nbTiles : Test(2) { 
    my ($w, $h) = HofOSM::TransImg::nbTiles(512, 768);
    is($w , 2, "nbTiles for 512 : $w" ); 
    is($h , 3, "nbTiles for 768 : $h" ); 
}

sub test_addNotice : Test(2) { 
    # Create a new Image
    # Take it signature 
    # Clone it
    # Compare the two signature
    # Add a notice to the second image
    # Compare the new signature with the original one

    my $imgA = Image::Magick->new;

    $imgA->Set(size=>"20x20", colorspace=>'RGB', depth=>'8', type=>'Palette', magick=>'PNG');   
    $imgA->ReadImage('xc:#f0f0f0');

    my $sigA = $imgA->Get('signature');
    my $imgB = $imgA->Clone();
    my $sigB1 = $imgB->Get('signature');

    HofOSM::TransImg::addNotice(\$imgB);

    my $sigB2 = $imgB->Get('signature');

    is ( ($sigA eq $sigB1) , 1, "add notice to the image");
    is ( ($sigA ne $sigB2) , 1, "add notice to the image");
}

sub test_imgCrop : Test(2) {

    my $imgA=Image::Magick->new;
    $imgA->Set(size=>'40x60', colorspace=>'RGB', depth=>'8',  type=>'Palette', magick=>'PNG');
    $imgA->Read('xc:red');

    $imgA->Draw(primitive=>'Polygon',fill=>'red', points=>'2,4 10,4 10,20 2,20');

    my ($width, $height) = (8,16);

    HofOSM::TransImg::imgCrop(\$imgA, $width ,$height, 2,4);

    is ($imgA->Get('width'), $width, "cropImage");
    is ($imgA->Get('height'), $height, "cropImage");

}

sub test_imgCompose : Test(3) {

    my $imgA=Image::Magick->new;
    $imgA->Set(size=>'40x20', colorspace=>'RGB', depth=>'8',  type=>'Palette', magick=>'PNG');
    $imgA->Read('xc:blue');

    my $imgB=Image::Magick->new;
    $imgB->Set(size=>'20x20', colorspace=>'RGB', depth=>'8',  type=>'Palette', magick=>'PNG');
    $imgB->Read('xc:red');

    my $imgC=Image::Magick->new;
    $imgC->Set(size=>'40x20', colorspace=>'RGB', depth=>'8',  type=>'Palette', magick=>'PNG');
    $imgC->Read('xc:red');

    HofOSM::TransImg::imgCompose(\$imgA, $imgB, 0 , 0);
    HofOSM::TransImg::imgCompose(\$imgA, $imgB, 20 , 0);

    is ($imgA->Get('height'), 20, "cropImage");
    is ($imgA->Get('width'), 40, "cropImage");

    my $sigA = $imgA->Get('signature');
    my $sigC = $imgC->Get('signature');

    is ( ( $sigA eq $sigC ), 1, "compose Image");
}

sub test_createNewImage : Test(1) { 
    my $foo = HofOSM::TransImg::createNewImage(1, 2);

    my $size = $foo->Get('size');

    is ($size, "256x512", "create new image");
}

1;
